/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

const port = 8080

func Test_healthcheck_ok(t *testing.T) {
	url := fmt.Sprintf("http://localhost:%d/health", port)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()

	Health(res, req)

	if res.Code != http.StatusOK {
		t.Fatalf("Response code was %v - %s", res.Code, res.Body.String())
	}
	var healthResponse healthResponse
	expectedErrorStatus := "OK"
	if err := json.Unmarshal(res.Body.Bytes(), &healthResponse); err != nil {
		t.Fatal(err)
	} else if healthResponse.Status != expectedErrorStatus {
		t.Fatalf("Invalid error status. Expected %s, got %s", expectedErrorStatus, healthResponse.Status)
	}
}

func Test_method_not_allowed(t *testing.T) {
	url := fmt.Sprintf("http://localhost:%d/health", port)
	methods := []string{"POST", "PATCH", "HEAD", "DELETE"}
	for _, method := range methods {
		req, err := http.NewRequest(method, url, nil)
		if err != nil {
			t.Fatal(err)
		}
		res := httptest.NewRecorder()

		Health(res, req)
		if res.Code != http.StatusMethodNotAllowed {
			t.Fatalf("Response code was %v for method %s and should be %d", res.Code, method, http.StatusMethodNotAllowed)
		}
	}
}
