/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"log"
	"os"
	"strconv"
)

// Environment String
type EnvStr string

func (env EnvStr) Or(def string) string {
	if v, ok := os.LookupEnv(string(env)); ok {
		return v
	}

	return def
}

func (env EnvStr) OrDie() string {
	v, ok := os.LookupEnv(string(env))
	if !ok {
		log.Fatalf("missing mandatory environment variable: %s", env)
	}

	return v
}

// Environment Int
type EnvInt string

func (env EnvInt) Or(def int) int {
	if v, ok := os.LookupEnv(string(env)); ok {
		if i, err := strconv.ParseInt(v, 10, 32); err == nil {
			return int(i)
		}

		return def
	}

	return def
}

func (env EnvInt) OrDie() int {
	v, ok := os.LookupEnv(string(env))
	if !ok {
		log.Fatalf("missing mandatory environment variable: %s", env)
	}

	i, err := strconv.ParseInt(v, 10, 32)
	if err != nil {
		log.Fatalf("unable to convert '%s' mandatory environment variable value '%s' to int: %s", env, v, err)
	}

	return int(i)
}

// Environment Bool
type EnvBool string

func (env EnvBool) Or(def bool) bool {
	if v, ok := os.LookupEnv(string(env)); ok {
		if b, err := strconv.ParseBool(v); err == nil {
			return b
		}

		return def
	}

	return def
}

func (env EnvBool) OrDie() bool {
	v, ok := os.LookupEnv(string(env))
	if !ok {
		log.Fatalf("missing mandatory environment variable: %s", env)
	}

	b, err := strconv.ParseBool(v)
	if err != nil {
		log.Fatalf("unable to convert '%s' mandatory environment variable value '%s' to bool: %s", env, v, err)
	}

	return b
}
